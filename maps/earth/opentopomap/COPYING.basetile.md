The base tile image `0/0/0.png` for "OpenTopoMap" is
* Map Data: © [OpenStreetMap](https://openstreetmap.org/copyright) contributors,
* Map style: © [OpenTopoMap](https://opentopomap.org/about), [Creative Commons Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)](https://creativecommons.org/licenses/by-sa/3.0/).
