The base tile image `0/0/0.png` for "ÖPNVKarte" is
* Map Data: © [OpenStreetMap](https://wiki.openstreetmap.org/wiki/OpenStreetMap) Contributors, licensed unter the [Open Databae License (ODbL)](http://opendatacommons.org/licenses/odbl/1.0/).
* Map style: © [MeMoMaps](https://memomaps.de/), [Creative Commons Attribution-ShareAlike 2.0 Generic (CC BY-SA 2.0)](http://creativecommons.org/licenses/by-sa/2.0/).
