# marble-maps-extra

This provides additional map definitions for "[KDE Marble](https://apps.kde.org/marble/)".

See the [`maps/earth/`](./maps/earth/) directory for individual maps.

## Installation

### As user

Copy [`maps/*`](./maps/) into `${HOME}/.local/share/marble/maps/` (e.g. [`maps/earth/opentopomap`](./maps/earth/opentopomap) goes to `${HOME}/.local/share/marble/maps/earth/opentopomap`).

### System wide

Copy [`maps/*`](./maps/) into `/usr/share/marble/data/maps/` (e.g. [`maps/earth/opentopomap`](./maps/earth/opentopomap) goes to `/usr/share/marble/data/maps/earth/opentopomap`).

<!--#### Arch Linux based distributions

Install the package [`marble-maps-extra-git` from the AUR](https://aur.archlinux.org/packages/marble-maps-extra-git).-->

## License

For licensing see [`COPYING.md`](./COPYING.md).
